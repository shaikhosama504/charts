frappe.pages["my_page"].on_page_load = function (wrapper) {
	frappe.ui.make_app_page({
		parent: wrapper,
		title: __("my-page"),
		single_column: true,
	});
};

frappe.pages["my_page"].on_page_show = function (wrapper) {
	load_desk_page(wrapper);
};

function load_desk_page(wrapper) {
	let $parent = $(wrapper).find(".layout-main-section");
	$parent.empty();

	frappe.require("my_page.bundle.js").then(() => {
		frappe.my_page = new frappe.ui.MyPage({
			wrapper: $parent,
			page: wrapper.page,
		});
	});
}